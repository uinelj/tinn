extern crate rulinalg;
extern crate tinynn;

use tinynn::nn::Layer;
use tinynn::nn::Network;
use tinynn::utils::mse;

use rulinalg::matrix::Matrix;

fn main() {
    // Variables
    let mut net = Network::new(vec![Layer::new(2, 20), Layer::new(20, 1)]);
    let target_mse = 0.01;
    let learning_rate = 0.1;
    let mut errors: Vec<Matrix<f64>> = Vec::new();
    let mut error: Matrix<f64>;
    let mut output: Matrix<f64>;

    // XOR data
    let data = vec![
        (
            Matrix::new(1, 2, vec![0f64, 0f64]),
            Matrix::new(1, 1, vec![0f64]),
        ),
        (
            Matrix::new(1, 2, vec![0f64, 1f64]),
            Matrix::new(1, 1, vec![1f64]),
        ),
        (
            Matrix::new(1, 2, vec![1f64, 0f64]),
            Matrix::new(1, 1, vec![1f64]),
        ),
        (
            Matrix::new(1, 2, vec![1f64, 1f64]),
            Matrix::new(1, 1, vec![0f64]),
        ),
    ];

    println!("Training: ");
    for epoch in 0..5000 {
        errors.clear();
        for input in &data {
            output = net.forward(&input.0);
            error = &input.1 - &output;
            errors.push(error.clone());
            net.backprop(&input.0, &error, learning_rate);
        }

        if epoch % 100 == 0 {
            let squared_error = mse(&errors);
            println!(
                "  Epoch {}   MSE: {}",
                epoch,
                format!("{:.*}", 3, squared_error)
            );
            if squared_error <= target_mse {
                println!("  * Target MSE reached *");
                break;
            }
        }
    }

    println!("Evaluating: ");

    let mut pred: f64;
    for input in data {
        pred = net.forward(&input.0)[[0, 0]];
        println!(
            "  {} XOR {} = {} ( {}) Error: {}",
            input.0[[0, 0]],
            input.0[[0, 1]],
            pred.round(),
            format!("{:.*}", 3, pred),
            format!("{:.*}", 3, input.1[[0, 0]] - pred),
        );
    }
}
