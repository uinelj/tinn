extern crate rand;
extern crate rulinalg;

use rand::thread_rng;
use rand::Rng;
use rulinalg::matrix::BaseMatrix;
use rulinalg::matrix::BaseMatrixMut;
use rulinalg::matrix::Matrix;

fn tanh(x: f64) -> f64 {
    x.tanh()
}

fn dtanh(x: f64) -> f64 {
    1f64 - x.powi(2)
}

/// Layer of a 
pub struct Layer {
    weights: Matrix<f64>,
    output: Matrix<f64>,
    delta: Matrix<f64>,
}

/// Multi-layer Network
///
/// # Example
/// 
/// ```
/// use tinynn::nn::Network;
/// 
/// let mut net = Network::new(
///    vec![
///        Layer::new(2, 20),
///        Layer::new(20, 20), 
///        Layer::new(20, 1)]
///    );
///```
pub struct Network {
    layers: Vec<Layer>,
}

impl Layer {
    pub fn new(num_inputs: usize, num_outputs: usize) -> Layer {
        Layer {
            weights: Matrix::from_fn(num_inputs, num_outputs, |_, _| {
                thread_rng().gen_range(-1f64, 1f64)
            }),
            output: Matrix::zeros(1, num_outputs),
            delta: Matrix::zeros(1, num_outputs),
        }
    }

    /// Compute the forward propagation of input
    ///
    /// `output = tanh(input*weights)`
    pub fn forward(&mut self, input: &Matrix<f64>) -> Matrix<f64> {
        let result = (input * &self.weights).apply(&tanh);
        self.output = result.clone();
        result
    }

    /// Compute the gradient of the error
    ///
    /// `delta = error ∘ dtanh(output)`
    /// `output = delta * transposed(weights)`
    fn compute_gradient(&mut self, error: &Matrix<f64>) -> Matrix<f64> {
        self.delta = error.elemul(&self.output.clone().apply(&dtanh));
        &self.delta * &self.weights.clone().transpose()
    }

    /// Update weights
    ///
    /// `W += transposed(input) * delta * learning_rate`
    fn update_weights(&mut self, input: &Matrix<f64>, learning_rate: f64) {
        let update = input.clone().transpose() * self.delta.clone() * learning_rate;
        self.weights += update;
    }
}

impl Network {
    pub fn new(layers: Vec<Layer>) -> Network {
        Network { layers }
    }

    pub fn forward(&mut self, input: &Matrix<f64>) -> Matrix<f64> {
        let mut output = input.clone();
        for layer in &mut self.layers {
            output = layer.forward(&output);
        }
        output
    }

    pub fn backprop(&mut self, input: &Matrix<f64>, error: &Matrix<f64>, learning_rate: f64) {
        let mut input_tmp = input.clone();
        let mut error_tmp = error.clone();

        let mut layers_rev = self.layers.iter_mut().rev();

        for layer in &mut layers_rev {
            error_tmp = layer.compute_gradient(&error_tmp);
        }
        for layer in &mut self.layers {
            layer.update_weights(&input_tmp, learning_rate);
            input_tmp = layer.output.clone();
        }
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
