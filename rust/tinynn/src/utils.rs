extern crate rulinalg;

use rulinalg::matrix::Matrix;

pub fn mse(errors: &[Matrix<f64>]) -> f64 {
    errors
        .iter()
        .map(|x| x * x)
        .fold(0f64, |acc, x| acc + x[[0, 0]])
        / errors.len() as f64
}
