
import numpy as np
import nn

# Training examples
examples = [
  #             _ XOR _       =          _
  [ np.array([[ 0,    0 ]]), np.array([[ 0 ]]) ],
  [ np.array([[ 0,    1 ]]), np.array([[ 1 ]]) ],
  [ np.array([[ 1,    0 ]]), np.array([[ 1 ]]) ],
  [ np.array([[ 1,    1 ]]), np.array([[ 0 ]]) ]
]



# Build the model
num_inputs = 2
num_hidden = 20 # nodes in hidden layer
num_output = 1
network = nn.Network([
    nn.Layer(num_inputs, num_hidden),
    nn.Layer(num_hidden, num_output)
  ])


# Train the model
print("Training:")
learning_rate = 0.1
target_mse = 0.01

for epoch in range(500):
  errors = []

  for input, target in examples:
    # Forward
    output = network.forward(input)

    # Compute the error
    error = target - output
    errors.append(error)

    # Back-propagate the error
    network.backprop(input, error, learning_rate)

  # Compute the Mean Squared Error of all examples each 100 epoch
  if epoch % 100 == 0:
    mse = (np.array(errors) ** 2).mean()
    print("  Epoch %3d   MSE: %.3f" % (epoch, mse))
    if mse <= target_mse:
      print("  * Target MSE reached *")
      break


# Evaluate the model
def eval(x, y):
  output = network.forward(x)
  normalized = int(round(output[0]))
  error = y - output[0]
  return "%d (% .3f)   Error: %.3f" % (normalized, output[0], error)

print("Evaluating:")
print("  1 XOR 0 = " + eval(np.array([1, 0]), 1))
print("  0 XOR 1 = " + eval(np.array([0, 1]), 1))
print("  1 XOR 1 = " + eval(np.array([1, 1]), 0))
print("  0 XOR 0 = " + eval(np.array([0, 0]), 0))
